<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	$random_bg = array("#27ae60", "#e67e22", "#e74c3c");
    return view('rooms', compact('random_bg'));
});

Route::get('/used', function () {
	$content = file_get_contents(public_path("used_count.txt"));

	return $content;
});