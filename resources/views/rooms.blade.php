<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>DevThis2</title>

	<script type="text/javascript" src="/jquery.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="/bootstrap.min.js"></script>

	<script>
	$(function(){
		setInterval(function(){
			$.get('/used', function(result){
				if(result <= 5)
				{
					$('#installed').css("background", "#27ae60");
				}
				else
				{
					if(result <= 10)
					{
						$('#installed').css("background", "#e67e22");
					}
					else
					{
						if(result > 10)
						{
							$('#installed').css("background", "#e74c3c");
						}
					}
				}				
			});
		}, 500);
	});
	</script>

	<style>
		.room{
			border: 1px solid #000;
			height: 325px;
			padding: 20px;
			background: #27ae60;
			color: #fff;
		}
	</style>
</head>

<body>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<div class="room" style="background:{{ $random_bg[array_rand($random_bg)] }}">
					<h1>WN4017</h1>
				</div>

				<div class="room" id="installed">
					<h1>WN4019</h1>
				</div>

				<div class="room" style="background:{{ $random_bg[array_rand($random_bg)] }}">
					<h1>WN4021</h1>
				</div>
			</div>

			<div class="col-xs-4 col-xs-push-4">
				<div class="room" style="background:{{ $random_bg[array_rand($random_bg)] }}">
					<h1>WN4018</h1>
				</div>

				<div class="room" style="background:{{ $random_bg[array_rand($random_bg)] }}">
				<h1>WN4020</h1>
				</div>

				<div class="room" style="background:{{ $random_bg[array_rand($random_bg)] }}">
					<h1>WN4022</h1>
				</div>
			</div>
		</div>
	</div>


</body>

</html>